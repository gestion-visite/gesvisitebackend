﻿using Microsoft.AspNetCore.Mvc;
using MiniProject.Dtos;
using MiniProject.Repository.Company;

using MiniProject.Repository.User;

using MiniProject.Models;
using System.Text.Json;
using Microsoft.AspNetCore.Authorization;
using MiniProject.Helppers;
using System.Security.Claims;

namespace MiniProject.Controllers
{
    [Route("entreprise")]
    [ApiController]
    public class CompanyController : Controller
    {
        private readonly ICompanyRepository _Companyrepository;
        public readonly IUserRepository _Userrepository;

        public CompanyController(IUserRepository userrepository, ICompanyRepository companyrepository)
        {
            _Companyrepository = companyrepository;
            _Userrepository = userrepository;
        }

        [HttpGet, Authorize(Roles = "SuperAdmin,Gardien")]
        public IActionResult GetAllCompanys()
        {
            var companys = _Companyrepository.GetAllCompanys();
            var companyRes = new List<CompanyDtos>();
            foreach (var company in companys)
            {
                var user = _Userrepository.GetAdmin(company.Id);

                if (user != null) companyRes.Add(new CompanyDtos
                {
                    Id = company.Id,
                    Nom = company.Nom,
                    Telephone = company.Telephone,
                    Email = company.Email,
                    Adresse = company.Adresse,
                    Actif = company.Actif,
                    Administrator = new UserDtos
                    {
                        Nom = user.Nom,
                        Prenom = user.Prenom,
                        Telephone = user.Telephone,
                        Fonction = user.Fonction,
                        Email = user.Email,
                        Adresse = user.Adresse,
                        Type = user.Type,
                        Actif = user.Actif,
                        Idcompany = company.Id,
                    }
                });
            }
            return Json(companyRes);
        }

        [HttpGet("{id}"), Authorize(Roles = "SuperAdmin")]
        public IActionResult GetCompanyById(int id) => _Companyrepository.GetCompanyById(id) == null ? BadRequest(new { message = "Company Not Exist" }) : Ok(_Companyrepository.GetCompanyById(id));

        [HttpPost, Authorize(Roles = "SuperAdmin")]
        public async Task<IActionResult> AddAsync([FromBody] CompanyDtos companyDtos)
        {
            if (_Companyrepository.GetCompanyByEmail(companyDtos.Email) != null) return BadRequest(new { message = "Entreprise exist deja" });
            if (_Userrepository.GetUserByEmail(companyDtos.Administrator.Email!) != null) return BadRequest(new { message = "Utilisateur exist deja" });
            var company = new Company
            {
                Nom = companyDtos.Nom,
                Telephone = companyDtos.Telephone,
                Email = companyDtos.Email,
                Adresse = companyDtos.Adresse,
                Actif = companyDtos.Actif
            };
            company = _Companyrepository.Create(company);
            if (company == null) return BadRequest(new { message = "Error company" });
            var userDtos = companyDtos.Administrator;
            userDtos.Idcompany = company.Id;
            var user = new UserController(_Userrepository, _Companyrepository).Add(userDtos).Result;
            if (user == null) return BadRequest(new { message = "Error user" });
            return Created("succes", user);
        }

        [HttpPut("{id}"), Authorize(Roles = "SuperAdmin")]
        public IActionResult Update(int id, CompanyDtos companyDtos)
        {
            var company = _Companyrepository.GetCompanyById(id);
            if (company == null) return BadRequest(new { message = "Company Not Exist" });
            company.Adresse = companyDtos.Adresse;
            company.Actif = companyDtos.Actif;
            company.Telephone = companyDtos.Telephone;
            company.Email = companyDtos.Email;
            company.Nom = companyDtos.Nom;
            company = _Companyrepository.Update(company);
            var user = _Userrepository.GetAdmin(company.Id);
            user.Adresse = companyDtos.Administrator.Adresse;
            user.Actif = companyDtos.Administrator.Actif;
            user.Telephone = companyDtos.Administrator.Telephone;
            user.Prenom = companyDtos.Administrator.Prenom;
            user.Fonction = companyDtos.Administrator.Fonction;
            user.Email = companyDtos.Administrator.Email;
            user.Nom = companyDtos.Administrator.Nom;
            user.Type = companyDtos.Administrator.Type;
            _Userrepository.Update(user);

            return Ok(company);
        }

        [HttpPut("active/{id}"), Authorize(Roles = "SuperAdmin,Administrateur")]
        public IActionResult active(int id)
        {
            var company = _Companyrepository.GetCompanyById(id);
            if (company == null) return BadRequest(new { message = "Company Not Exist" });

            company.Actif = company.Actif == TypeActif.Actif ? TypeActif.Desactif : TypeActif.Actif;
            company = _Companyrepository.Update(company);

            return Ok(company);
        }

        [HttpDelete("{id}"), Authorize(Roles = "SuperAdmin")]
        public IActionResult Remove(int id)
        {
            Company company = _Companyrepository.GetCompanyById(id);
            if (company == null) return BadRequest(new { message = "Company Not Exist" });
            bool del = _Companyrepository.Delete(id);
            if (!del) return BadRequest(new { message = "Error On Delete Company" });
            return Ok("succes");
        }
    }
}