﻿using Microsoft.AspNetCore.Mvc;
using MiniProject.Repository.User;
using MiniProject.Repository.Company;
using MiniProject.Models;

using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using MiniProject.Helppers;

namespace MiniProject.Controllers
{
    [Route("authentification")]
    [ApiController]
    public class AuthentificationController : Controller
    {
        public readonly ICompanyRepository _Companyrepository;
        public readonly IUserRepository _Userrepository;
        public readonly IConfiguration _Configuration;
        public static User? compte;
        public static HttpContext cnx;

        public AuthentificationController(IConfiguration configuration, ICompanyRepository companyrepository, IUserRepository userrepository)
        {
            _Configuration = configuration;
            _Companyrepository = companyrepository;
            _Userrepository = userrepository;
        }

        [HttpPost]
        public async Task<ActionResult<string>> Login([FromForm] string email, [FromForm] string password)
        {
            cnx = HttpContext;
            if (email == "admin@super.com")
            {
                compte = new User
                {
                    Email = "admin@super.com"
                };
            }
            else
            {
                compte = _Userrepository.GetUserByEmail(email);
                if (compte == null || !Utils.VerifyPasswordHash(password, compte.PasswordHash, compte.PasswordSalt)) return NotFound("L'utilisateur spécifié ou le mot de passe saisi est incorrect, vérifiez la syntaxe et réessayez.");
                if (compte.Actif == 0) return BadRequest("Votre compte est ne pas actif pour le moment tu peux contact l'administrateur pour plus des informations");
            }

            string Token = CreateToken(compte, out string Fullname, out string Role, out int IDcompany, out string Namecompany);
            var refreshToken = GenerateRefreshToken();
            SetRefreshToken(refreshToken);

            return Ok(new
            {
                Token,
                Fullname,
                Role,
                IDcompany,
                Namecompany
            });
        }

        [HttpPost("refresh-token")]
        public async Task<ActionResult<string>> RefreshToken()
        {
            cnx = HttpContext;
            var refreshToken = Request.Cookies["refreshToken"];
            if (compte == null)
            {
                return Unauthorized("You most login first");
            }
            if (!compte.Token.Equals(refreshToken))
            {
                return Unauthorized("Invalid Refresh Token.");
            }
            else if (compte.TokenExpires < DateTime.Now)
            {
                return Unauthorized("Token expired.");
            }

            string token = CreateToken(compte, out string Fullname, out string role, out int IDcompany, out string Namecompany);
            var newRefreshToken = GenerateRefreshToken();
            SetRefreshToken(newRefreshToken);

            return Ok(new
            {
                token,
                Fullname,
                role,
                IDcompany,
                Namecompany
            });
        }

        public static RefreshToken GenerateRefreshToken()
        {
            var refreshToken = new RefreshToken
            {
                Token = Convert.ToBase64String(RandomNumberGenerator.GetBytes(64)),
                Expires = DateTime.Now.AddDays(7),
                Created = DateTime.Now
            };

            return refreshToken;
        }

        public static void SetRefreshToken(RefreshToken newRefreshToken)
        {
            var cookieOptions = new CookieOptions
            {
                HttpOnly = true,
                Expires = newRefreshToken.Expires
            };

            cnx.Response.Cookies.Append("refreshToken", newRefreshToken.Token, cookieOptions);
            compte.Token = newRefreshToken.Token;
            compte.TokenCreated = newRefreshToken.Created;
            compte.TokenExpires = newRefreshToken.Expires;
        }

        public static string CreateToken(User cpt, out string Fullname, out string role, out int IDcompany, out string Namecompany)
        {
            string email;
            if (cpt.Email != "admin@super.com")
            {
                role = cpt.Type.ToString();
                email = cpt.Email;
                Fullname = cpt.Nom + " " + cpt.Prenom;
                if (cpt.Type == TypeUser.Gardien)
                {
                    IDcompany = -1;
                    Namecompany = "";
                }
                else
                {
                    IDcompany = cpt.Company!.Id;
                    Namecompany = cpt.Company!.Nom;
                }
            }
            else
            {
                role = "SuperAdmin";
                email = "admin@super.com";
                Fullname = "Super Admin";
                IDcompany = -1;
                Namecompany = "";
            }
            List<Claim> claims = new()
            {
                new Claim(ClaimTypes.Name,email),
                new Claim(ClaimTypes.Role,role ),
                new Claim("IDcompany",IDcompany.ToString())
            };

            var key = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes("neoledge mini project"));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: creds);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}