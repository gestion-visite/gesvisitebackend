﻿using Microsoft.AspNetCore.Mvc;
using MiniProject.Repository.Visit;
using MiniProject.Repository.Company;
using MiniProject.Models;
using MiniProject.Dtos;
using Microsoft.AspNetCore.Authorization;

namespace MiniProject.Controllers
{
    [Route("visit")]
    [ApiController]
    public class VisitController : Controller
    {
        private readonly IVisitRepository _Visitrepository;
        private readonly ICompanyRepository _Companyrepository;

        public VisitController(IVisitRepository visitrepository, ICompanyRepository companyrepository)
        {
            _Visitrepository = visitrepository;
            _Companyrepository = companyrepository;
        }

        [HttpGet("all"), Authorize(Roles = "Gardien,Administrateur")]
        public IActionResult GetAllVisits(int id) => Ok(_Visitrepository.GetAllVisits(id));

        [HttpGet("{id}"), Authorize(Roles = "Gardien,Administrateur")]
        public IActionResult GetVisitById(int id) => _Visitrepository.GetVisitById(id) == null ? BadRequest(new { message = "Visit Not Exist" }) : Ok(_Visitrepository.GetVisitById(id));

        [HttpPost, Authorize(Roles = "Administrateur")]
        public IActionResult Add(VisitDtos visitDtos)
        {
            var company = _Companyrepository.GetCompanyById(visitDtos.Idcompany);
            var visit = new Visit
            {
                Visitor = visitDtos.visitor,
                Company = company,
                DateVisit = visitDtos.DateVisit
            };
            visit = _Visitrepository.Create(visit);
            if (visit == null) return BadRequest(new { message = "Error" });
            return Created("succes", visit);
        }

        [HttpPut("{id}"), Authorize(Roles = "Administrateur")]
        public IActionResult Update(int id, VisitDtos visitDtos)
        {
            var visit = _Visitrepository.GetVisitById(id);
            if (visit == null) return BadRequest(new { message = "Visit Not Exist" });
            var company = _Companyrepository.GetCompanyById(visitDtos.Idcompany);
            visit.Company = company!;
            visit.DateVisit = visitDtos.DateVisit;
            visit.Visitor = visitDtos.visitor;
            visit = _Visitrepository.Update(visit);
            return Ok(visit);
        }

        [HttpPut("access/{id}"), Authorize(Roles = "Gardien")]
        public IActionResult SetAccess(int id) => Ok(_Visitrepository.Access(id));

        [HttpDelete("{id}"), Authorize(Roles = "Administrateur")]
        public IActionResult Remove(int id)
        {
            Visit visit = _Visitrepository.GetVisitById(id);
            if (visit == null) return BadRequest(new { message = "Visit Not Exist" });
            bool del = _Visitrepository.Delete(id);
            if (!del) return BadRequest(new { message = "Error On Delete Visit" });
            return Ok("succes");
        }
    }
}