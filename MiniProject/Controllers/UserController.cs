﻿using Microsoft.AspNetCore.Mvc;
using MiniProject.Dtos;
using MiniProject.Models;
using MiniProject.Repository.User;
using MiniProject.Repository.Company;
using System.Text.Json;
using Microsoft.AspNetCore.Authorization;
using MiniProject.Helppers;
using System.Security.Claims;

namespace MiniProject.Controllers
{
    [Route("utilisateur")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly IUserRepository _Userrepository;
        private readonly ICompanyRepository _Companyrepository;

        public UserController(IUserRepository userrepository, ICompanyRepository companyrepository)
        {
            _Userrepository = userrepository;
            _Companyrepository = companyrepository;
        }

        [HttpGet, Authorize(Roles = "SuperAdmin,Administrateur,Gardien,entreprise")]
        public IActionResult GetAllUsers()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            var value = identity?.FindFirst("IDcompany")?.Value;
            int.TryParse(value, out int IdCompany);
            if (IdCompany == -1)
            {
                return Ok(_Userrepository.GetAllGuardian());
            }
            else
            {
                return Ok(_Userrepository.GetAllUsers(IdCompany));
            }
        }

        [HttpGet("admin/{id}")]
        public IActionResult GetAdmin(int id) => Ok(_Userrepository.GetAdmin(id));

        [HttpGet("{id}"), Authorize(Roles = "SuperAdmin,Administrateur,Gardien")]
        public IActionResult GetUserById(int id) => _Userrepository.GetUserById(id) == null ? BadRequest(new { message = "User Not Exist" }) : Ok(_Userrepository.GetUserById(id));

        [HttpPost, Authorize(Roles = "SuperAdmin,Administrateur")]
        public async Task<IActionResult> Add(UserDtos userDtos)
        {
            Utils.CreatePasswordHash(userDtos.Password, out byte[] passwordHash, out byte[] passwordSalt);
            var company = _Companyrepository.GetCompanyById(userDtos.Idcompany);
            if (company == null && userDtos.Type != TypeUser.Gardien) return BadRequest(new { message = "Error Company Not Found" });
            var user = new User
            {
                Nom = userDtos.Nom,
                Prenom = userDtos.Prenom,
                Telephone = userDtos.Telephone,
                Fonction = userDtos.Fonction,
                Email = userDtos.Email,
                Adresse = userDtos.Adresse,
                Type = userDtos.Type,
                Actif = userDtos.Actif,
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt
            };
            if (company != null) user.Company = company;
            user = _Userrepository.Create(user);
            if (user == null) return BadRequest(new { message = "Error" });
            return Created("succes", _Userrepository.Create(user));
        }

        [HttpPut("{id}"), Authorize(Roles = "SuperAdmin,Administrateur")]
        public IActionResult Update(int id, UserDtos userDtos)
        {
            var user = _Userrepository.GetUserById(id);
            if (user == null) return BadRequest(new { message = "User Not Exist" });
            user.Adresse = userDtos.Adresse;
            user.Actif = userDtos.Actif;
            user.Telephone = userDtos.Telephone;
            user.Prenom = userDtos.Prenom;
            user.Fonction = userDtos.Fonction;
            user.Email = userDtos.Email;
            user.Nom = userDtos.Nom;
            user.Type = userDtos.Type;
            user = _Userrepository.Update(user);
            return Ok(user);
        }

        [HttpPut("active/{id}"), Authorize(Roles = "SuperAdmin,Administrateur")]
        public IActionResult active(int id)
        {
            var user = _Userrepository.GetUserById(id);
            if (user == null) return BadRequest(new { message = "User Not Exist" });

            user.Actif = user.Actif == TypeActif.Actif ? TypeActif.Desactif : TypeActif.Actif;
            user = _Userrepository.Update(user);

            return Ok(user);
        }

        [HttpDelete("{id}"), Authorize(Roles = "SuperAdmin,Administrateur")]
        public IActionResult Remove(int id)
        {
            User user = _Userrepository.GetUserById(id);
            if (user == null) return BadRequest(new { message = "User Not Exist" });
            bool del = _Userrepository.Delete(id);
            if (!del) return BadRequest(new { message = "Error On Delete User" });
            return Ok("succes");
        }
    }
}