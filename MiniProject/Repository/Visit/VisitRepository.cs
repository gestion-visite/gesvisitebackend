﻿using Microsoft.EntityFrameworkCore;
using MiniProject.Data;

namespace MiniProject.Repository.Visit
{
    public class VisitRepository : IVisitRepository
    {
        private readonly BaseContext _context;

        public VisitRepository(BaseContext context)
        {
            _context = context;
        }

        bool IVisitRepository.Access(int id)
        {
            var res = _context.Visits.FirstOrDefault(p => p.Id == id);
            if (res == null) return false;
            res.isAccessed = true;
            _context.Entry(res!).CurrentValues.SetValues(res);
            _context.SaveChanges();
            return true;
        }

        Models.Visit IVisitRepository.Create(Models.Visit? visit)

        {
            _context.Visitors.Add(visit!.Visitor);
            _context.Visits.Add(visit!);
            _context.SaveChanges();
            return visit!;
        }

        bool IVisitRepository.Delete(int id)
        {
            var visit = _context.Visits.FirstOrDefault(p => p.Id == id);
            _context.Visits.Remove(visit!);
            if (_context.SaveChanges() == -1) return false;
            return true;
        }

        List<Models.Visit> IVisitRepository.GetAllVisits(int id) => _context.Visits.Include(v => v.Visitor).Where(v => v.Company.Id == id).ToList();

        Models.Visit IVisitRepository.GetVisitById(int id) => _context.Visits.FirstOrDefault(p => p.Id == id)!;

        Models.Visit IVisitRepository.Update(Models.Visit visit)
        {
            var visitorReq = visit.Visitor;
            var res = _context.Visits.FirstOrDefault(p => p.Id == visit.Id);

            _context.Entry(res!).CurrentValues.SetValues(visit);
            _context.SaveChanges();
            return res!;
        }
    }
}