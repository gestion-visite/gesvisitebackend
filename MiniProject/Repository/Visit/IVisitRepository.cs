﻿namespace MiniProject.Repository.Visit
{
    public interface IVisitRepository
    {
        Models.Visit Create(Models.Visit visit);

        Models.Visit Update(Models.Visit visit);

        bool Delete(int id);

        List<Models.Visit> GetAllVisits(int id);

        Models.Visit GetVisitById(int id);

        bool Access(int id);
    }
}