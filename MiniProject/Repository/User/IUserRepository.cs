﻿namespace MiniProject.Repository.User
{
    public interface IUserRepository
    {
        Models.User Create(Models.User user);

        Models.User Update(Models.User user);

        bool Delete(int id);

        List<Models.User> GetAllUsers(int IdCompany);

        List<Models.User> GetAllGuardian();

        Models.User GetUserByEmail(string email);

        Models.User GetUserById(int id);

        Models.User GetAdmin(int id);
    }
}