﻿using Microsoft.EntityFrameworkCore;
using MiniProject.Data;

namespace MiniProject.Repository.User
{
    public class UserRepository : IUserRepository
    {
        private readonly BaseContext _context;

        public UserRepository(BaseContext context)
        {
            _context = context;
        }

        Models.User IUserRepository.Create(Models.User? user)
        {
            try
            {
                _context.Users.Add(user!);
                user!.Id = _context.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                Console.WriteLine(e.Message);
                user = null;
            }

            return user!;
        }

        bool IUserRepository.Delete(int id)
        {
            var user = _context.Users.FirstOrDefault(p => p.Id == id);
            _context.Users.Remove(user!);
            if (_context.SaveChanges() == -1) return false;
            return true;
        }

        List<Models.User> IUserRepository.GetAllUsers(int IdCompany) => _context.Users.Where(u => u.Company!.Id == IdCompany && u.Type == Models.TypeUser.Personnel).Include(u => u.Company).ToList();

        List<Models.User> IUserRepository.GetAllGuardian() => _context.Users.Include(u => u.Company).Where(u => u.Type == Models.TypeUser.Gardien).ToList();

        Models.User IUserRepository.GetUserByEmail(string email) => _context.Users.Include(u => u.Company).FirstOrDefault(p => p.Email == email)!;

        Models.User IUserRepository.GetUserById(int id) => _context.Users.Include(u => u.Company).FirstOrDefault(p => p.Id == id)!;

        Models.User IUserRepository.GetAdmin(int id) => _context.Users.Include(u => u.Company).Where(p => p.Type == Models.TypeUser.Administrateur).FirstOrDefault(p => p.Company!.Id == id)!;

        Models.User IUserRepository.Update(Models.User user)
        {
            var res = _context.Users.FirstOrDefault(p => p.Id == user.Id);
            _context.Entry(res!).CurrentValues.SetValues(user);
            _context.SaveChanges();
            return res!;
        }
    }
}