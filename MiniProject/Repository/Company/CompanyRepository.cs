﻿using Microsoft.EntityFrameworkCore;
using MiniProject.Data;

namespace MiniProject.Repository.Company
{
    public class CompanyRepository : ICompanyRepository
    {
        private readonly BaseContext _context;

        public CompanyRepository(BaseContext context)
        {
            _context = context;
        }

        Models.Company ICompanyRepository.Create(Models.Company? company)
        {
            try
            {
                _context.Companys.Add(company!);
                _context.SaveChanges();
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                Console.WriteLine(e.Message);
                company = null;
            }

            return company!;
        }

        bool ICompanyRepository.Delete(int id)
        {
            var company = _context.Companys.FirstOrDefault(p => p.Id == id);
            _context.Companys.Remove(company!);
            if (_context.SaveChanges() == -1) return false;
            return true;
        }

        List<Models.Company> ICompanyRepository.GetAllCompanys() => _context.Companys.ToList();

        Models.Company ICompanyRepository.GetCompanyByEmail(string email) => _context.Companys.FirstOrDefault(p => p.Email == email)!;

        Models.Company ICompanyRepository.GetCompanyById(int id) => _context.Companys.FirstOrDefault(p => p.Id == id)!;

        Models.Company ICompanyRepository.Update(Models.Company company)
        {
            var res = _context.Companys.FirstOrDefault(p => p.Id == company.Id);
            _context.Entry(res!).CurrentValues.SetValues(company);
            _context.SaveChanges();
            return res!;
        }
    }
}