﻿
namespace MiniProject.Repository.Company
{
    public interface ICompanyRepository
    {
        Models.Company Create(Models.Company company);
        Models.Company Update(Models.Company company);
        bool Delete(int id);
        List<Models.Company> GetAllCompanys();
        Models.Company GetCompanyByEmail(string email);
        Models.Company GetCompanyById(int id);
    }

}
