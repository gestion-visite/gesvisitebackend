﻿using Microsoft.EntityFrameworkCore;
using MiniProject.Models;

namespace MiniProject.Data
{
    public class BaseContext : DbContext
    {
        public BaseContext(DbContextOptions<BaseContext> contextOptions) : base(contextOptions)
        {
        }

        public DbSet<Company> Companys { set; get; }
        public DbSet<User> Users { set; get; }
        public DbSet<Visitor> Visitors { set; get; }
        public DbSet<Visit> Visits { set; get; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<User>()
                 .Property(e => e.Type)
                 .HasConversion(
                     v => v.ToString(),
                     v => (TypeUser)Enum.Parse(typeof(TypeUser), v));
            builder.Entity<User>(entity => { entity.HasIndex(e => e.Email).IsUnique(); });
            builder.Entity<Company>(entity => { entity.HasIndex(e => e.Email).IsUnique(); });
            builder.Entity<Visit>()
                .HasOne(s => s.Company)
                .WithMany(c => c.Visits);
            builder.Entity<Visit>()
               .HasOne(s => s.Visitor)
               .WithMany(c => c.Visits);
            builder.Entity<User>()
                .HasOne(c => c.Company)
                .WithMany(e => e.Users).OnDelete(DeleteBehavior.Cascade).IsRequired(false);
        }
    }
}