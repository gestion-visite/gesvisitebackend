using System.Text.Json.Serialization;

namespace MiniProject.Models
{
    public class User : Compte
    {
        public int Id { get; set; }

        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Telephone { get; set; }
        public string Fonction { get; set; }

        public string Adresse { get; set; }

        [JsonConverter(typeof(JsonStringEnumConverter))]
        public TypeUser Type { get; set; }

        public TypeActif Actif { get; set; }
        public virtual Company? Company { get; set; }
    }
}