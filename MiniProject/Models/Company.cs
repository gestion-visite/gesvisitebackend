using System.Text.Json.Serialization;

namespace MiniProject.Models
{
    public class Company
    {
        public int Id { get; set; }

        public string Nom { get; set; }
        public string Telephone { get; set; }

        public string Email { get; set; }
        public string Adresse { get; set; }
        public TypeActif Actif { get; set; }

        [JsonIgnore]
        public ICollection<User>? Users { get; set; }

        [JsonIgnore]
        public virtual ICollection<Visit>? Visits { get; set; }
    }
}