﻿namespace MiniProject.Models
{
    public class Visit
    {
        public int Id { get; set; }
        public Company Company { get; set; }
        public Visitor Visitor { get; set; }
        public DateTime DateVisit { get; set; }
        public bool isAccessed { get; set; }
    }
}