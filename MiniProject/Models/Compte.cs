﻿using System.Text.Json.Serialization;

namespace MiniProject.Models
{
    public class Compte
    {
        public string Email { get; set; }

        [JsonIgnore]
        public byte[] PasswordHash { get; set; }

        [JsonIgnore]
        public byte[] PasswordSalt { get; set; }

        [JsonIgnore]
        public string Token { get; set; } = string.Empty;

        [JsonIgnore]
        public DateTime TokenCreated { get; set; }

        [JsonIgnore]
        public DateTime TokenExpires { get; set; }
    }
}