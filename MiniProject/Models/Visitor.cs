﻿namespace MiniProject.Models
{
    public class Visitor
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }

        [System.Text.Json.Serialization.JsonIgnore]
        public virtual ICollection<Visit>? Visits { get; set; }
    }
}
