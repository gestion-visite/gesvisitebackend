﻿using MiniProject.Models;
using System.Text.Json.Serialization;

namespace MiniProject.Dtos
{
    public class VisitDtos
    {
        public Visitor visitor{ get; set; }
        public int Idcompany { get; set; }
        public DateTime DateVisit { get; set; }

    }

}
