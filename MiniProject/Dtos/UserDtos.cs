﻿using MiniProject.Models;
using System.Text.Json.Serialization;

namespace MiniProject.Dtos
{
    public class UserDtos
    {
        public string? Nom { get; set; }
        public string? Prenom { get; set; }
        public string? Telephone { get; set; }
        public string? Fonction { get; set; }
        public string? Password { get; set; }

        public string? Email { get; set; }
        public string? Adresse { get; set; }

        [JsonConverter(typeof(JsonStringEnumConverter))]
        public TypeUser Type { get; set; }

        [JsonConverter(typeof(JsonStringEnumConverter))]
        public TypeActif Actif { get; set; }

        public int Idcompany { get; set; }
    }
}